package com.vega.firstspringdata.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.vega.firstspringdata.entity.Student;

public interface StudentRepository extends MongoRepository<Student, String>{
	
	List<Student> findAll();

}
