package com.vega.firstspringdata.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vega.firstspringdata.entity.Student;
import com.vega.firstspringdata.repository.StudentRepository;

@RestController
public class StudentController {
	
	@Autowired
	StudentRepository studentRepository;
	
	@RequestMapping(value="/student", method= RequestMethod.GET)
	public List<Student> listStudent(){
		return this.studentRepository.findAll();
	}
	
	@RequestMapping(value="/student", method= RequestMethod.POST)
	public Student saveStudent(@RequestBody Student student) {
		return this.studentRepository.save(student);
	}
	

}
